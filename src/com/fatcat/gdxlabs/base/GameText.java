package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 27/04/2015.
 */
public class GameText extends GameObject implements IDrawable
{
   private FontDrawable mDrawable;
   private String mText;

   public GameText(Vector2 position, BitmapFont font, String text)
   {
      super(position);
      mDrawable = new FontDrawable(this, font);
      mDrawable.setText(text);
      mText = text;
   }

   public GameText(Vector2 position, BitmapFont font, String text, Drawable.Alignment align)
   {
      this(position, font, text);
      mDrawable.setAlignment(align);
   }

   public String text()
   {
      return mText;
   }

   public GameText text(String text)
   {
      mText = text;
      mDrawable.setText(text);
      return this;
   }

   @Override
   public FontDrawable getDrawable()
   {
      return mDrawable;
   }

}
