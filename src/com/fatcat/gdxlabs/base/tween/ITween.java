package com.fatcat.gdxlabs.base.tween;

import com.fatcat.gdxlabs.base.IUpdateable;

/**
 * Created by Tom on 26/04/2015.
 */
public interface ITween extends IUpdateable
{
   public void tweenStart();
   public void tweenEnd();
   public boolean isTweenFinished();
   public boolean isActive();
   public void setActive(boolean active);
}
