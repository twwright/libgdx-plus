package com.fatcat.gdxlabs.base.tween;

import com.badlogic.gdx.math.Interpolation;
import com.fatcat.gdxlabs.base.Utils;

/**
 * Created by Tom on 26/04/2015.
 */
public class FloatTween extends Tween<Float>
{
   public FloatTween(Float start, Float end, float time, Interpolation interpolation)
   {
      super(start, end, time, interpolation);
   }

   public FloatTween(Float start, Float end, float time)
   {
      super(start, end, time);
   }

   @Override
   public void tween(float alpha)
   {
      mValue = Utils.lerp(mStart, mEnd, alpha);
   }

   @Override
   public void tweenStart()
   {
      super.tweenStart();
      mValue = mStart;
   }

   @Override
   public void tweenEnd()
   {
      super.tweenEnd();
      mValue = mEnd;
   }

   @Override
   public Float defaultVal()
   {
      return 0f;
   }

   public FloatTween reverse()
   {
      return new FloatTween(mEnd, mStart, mTime, mInterpolation);
   }
}
