package com.fatcat.gdxlabs.base.tween;

/**
 * Created by Tom on 17/05/2015.
 */
public interface IEventListener
{
   public void onEvent(String event, Object data);
}
