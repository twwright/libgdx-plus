package com.fatcat.gdxlabs.base.tween;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Tom on 17/05/2015.
 */
public class EventManager
{
   private HashMap<String, ArrayList<IEventListener>> mEventMap = new HashMap<String, ArrayList<IEventListener>>();

   public void registerListener(String event, IEventListener listener)
   {
      if(!mEventMap.containsKey(event))
      {
         ArrayList<IEventListener> listeners = new ArrayList<IEventListener>();
         listeners.add(listener);
         mEventMap.put(event, listeners);
      }
      else if(!mEventMap.get(event).contains(listener))
      {
         mEventMap.get(event).add(listener);
      }
   }

   public void clear()
   {
      mEventMap.clear();
   }
   public ArrayList<IEventListener> clear(String event)
   {
      return mEventMap.remove(event);
   }

   public void fireEvent(String event)
   {
      fireEvent(event, null);
   }
   public void fireEvent(String event, Object data)
   {
      if(mEventMap.containsKey(event))
      {
         ArrayList<IEventListener> listeners = mEventMap.get(event);
         for(IEventListener listener : listeners)
            listener.onEvent(event, data);
      }
   }
}
