package com.fatcat.gdxlabs.base.tween;

import com.badlogic.gdx.math.Interpolation;
import com.fatcat.gdxlabs.base.GameWorld;

/**
 * Created by Tom on 26/04/2015.
 */
public abstract class Tween<T> extends BaseTween
{
   protected T mValue;
   protected T mStart;
   protected T mEnd;
   protected float mTime;
   protected float mElapsedTime;
   protected Interpolation mInterpolation;

   public Tween(T val, T start, T end, float time, Interpolation interpolation)
   {
      super();
      mValue = val;
      mStart = start;
      mEnd = end;
      mTime = time;
      mElapsedTime = 0;
      mInterpolation = interpolation;
   }

   public Tween(T val, T start, T end, float time)
   {
      this(val, start, end, time, Interpolation.linear);
   }

   public Tween(T start, T end, float time, Interpolation interpolation)
   {
      super();
      mValue = defaultVal();
      mStart = start;
      mEnd = end;
      mTime = time;
      mElapsedTime = 0;
      mInterpolation = interpolation;
   }

   public Tween(T start, T end, float time)
   {
      this(start, end, time, Interpolation.linear);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      mElapsedTime += delta;
      if(mElapsedTime < mTime)
      {
         tween(mInterpolation.apply(mElapsedTime / mTime));
         for(ITweenCallback<T> callback : callbacks)
            callback.onTween(mValue, this);
      }
   }

   public void tweenStart()
   {
      mElapsedTime = 0;
      super.tweenStart();
      for(ITweenCallback<T> callback : callbacks)
         callback.onTween(mStart, this);
   }

   public void tweenEnd()
   {
      for(ITweenCallback<T> callback : callbacks)
         callback.onTween(mEnd, this);
      super.tweenEnd();
   }

   public boolean isTweenFinished()
   {
      return mElapsedTime >= mTime;
   }

   public abstract void tween(float alpha);

   public abstract T defaultVal();

   public float getElapsedTime()
   {
      return mElapsedTime;
   }

   public void setElapsedTime(float time)
   {
      mElapsedTime = time;
   }

   public void set(T start, T end)
   {
      mStart = start;
      mEnd = end;
   }

   public T start() { return mStart; }
   public T end() { return mEnd; }

   public void setInterpolation(Interpolation terp)
   {
      mInterpolation = terp;
   }

   public void setTime(float time) { mTime = time; }
}
