package com.fatcat.gdxlabs.base.tween;

/**
 * Created by Tom on 17/05/2015.
 */
public abstract class TimerCallback implements ITweenCallback<Float>
{
   @Override
   public void onTweenStart(ITween tween)
   {
      // nothing
   }

   @Override
   public void onTween(Float val, ITween tween)
   {

   }

   @Override
   public void onTweenFinish(ITween tween)
   {
      onTimerFinish();
   }

   public abstract void onTimerFinish();
}
