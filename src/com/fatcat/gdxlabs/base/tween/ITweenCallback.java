package com.fatcat.gdxlabs.base.tween;

/**
 * Created by Tom on 26/04/2015.
 */
public interface ITweenCallback<T>
{
   public void onTweenStart(ITween tween);
   public void onTween(T val, ITween tween);
   public void onTweenFinish(ITween tween);
}
