package com.fatcat.gdxlabs.base.tween;

/**
 * Created by Tom on 26/04/2015.
 */
public class PauseTween extends Tween
{
   public PauseTween(float time)
   {
      super(null, null, time);
   }

   @Override
   public void tweenStart()
   {
      super.tweenStart();
   }

   @Override
   public void tweenEnd()
   {
      super.tweenEnd();
   }

   @Override
   public void tween(float alpha)
   {
      // Nothing
   }

   @Override
   public Object defaultVal()
   {
      return null;
   }
}
