package com.fatcat.gdxlabs.base.tween;

/**
 * Created by Tom on 27/04/2015.
 */
public class TweenCallback<T> implements ITweenCallback<T>
{
   @Override
   public void onTweenStart(ITween tween)
   {
      // Nothing
   }

   @Override
   public void onTween(T val, ITween tween)
   {
      // Nothing
   }

   @Override
   public void onTweenFinish(ITween tween)
   {
      // Nothing
   }
}
