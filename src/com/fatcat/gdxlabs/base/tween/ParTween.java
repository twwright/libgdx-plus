package com.fatcat.gdxlabs.base.tween;

import com.fatcat.gdxlabs.base.GameWorld;

/**
 * Created by Tom on 26/04/2015.
 */
public class ParTween extends BaseTween
{
   private BaseTween[] mTweens;
   private boolean mRepeat = false;

   public ParTween(BaseTween... tweens)
   {
      mTweens = tweens;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      for(BaseTween tween : mTweens)
      {
         if(!tween.isTweenFinished())
         {
            tween.update(world, delta);
            if(tween.isTweenFinished())
               tween.tweenEnd();
         }
      }
      if(isTweenFinished() && mRepeat)
         tweenStart();
   }

   @Override
   public void tweenStart()
   {
      for(BaseTween tween : mTweens)
         tween.tweenStart();
   }

   @Override
   public void tweenEnd()
   {
      super.tweenEnd();
   }

   @Override
   public boolean isTweenFinished()
   {
      boolean finished = true;
      for(BaseTween tween : mTweens)
         if(!tween.isTweenFinished())
            finished = false;
      return finished;
   }

   public boolean repeat()
   {
      return mRepeat;
   }

   public void repeat(boolean newRepeat)
   {
      mRepeat = newRepeat;
   }
}
