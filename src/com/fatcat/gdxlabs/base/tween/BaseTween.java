package com.fatcat.gdxlabs.base.tween;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 26/04/2015.
 */
public abstract class BaseTween implements ITween
{
   public final List<ITweenCallback> callbacks = new LinkedList<ITweenCallback>();
   private boolean mActive = false;

   @Override
   public void tweenStart()
   {
      for(ITweenCallback callback : callbacks)
         callback.onTweenStart(this);
   }
   @Override
   public void tweenEnd()
   {
      for(ITweenCallback callback : callbacks)
         callback.onTweenFinish(this);
   }

   public boolean isActive()
   {
      return mActive;
   }

   public void setActive(boolean active)
   {
      mActive = active;
   }
}
