package com.fatcat.gdxlabs.base.tween;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.fatcat.gdxlabs.base.Utils;

/**
 * Created by Tom on 26/04/2015.
 */
public class ColourTween extends Tween<Color>
{
   public ColourTween(Color start, Color end, float time)
   {
      super(start, end, time);
   }

   public ColourTween(Color start, Color end, float time, Interpolation interpolation)
   {
      super(start, end, time, interpolation);
   }

   public ColourTween(Color val, Color start, Color end, float time)
   {
      super(val, start, end, time);
   }

   public ColourTween(Color val, Color start, Color end, float time, Interpolation interpolation)
   {
      super(val, start, end, time, interpolation);
   }

   public Color defaultVal()
   {
      return Color.WHITE.cpy();
   }

   @Override
   public void tween(float alpha)
   {
      mValue.set(
            Utils.lerp(mStart.r, mEnd.r, alpha),
            Utils.lerp(mStart.g, mEnd.g, alpha),
            Utils.lerp(mStart.b, mEnd.b, alpha),
            Utils.lerp(mStart.a, mEnd.a, alpha)
      );
   }

   @Override
   public void tweenStart()
   {
      super.tweenStart();
      mValue.set(mStart);
   }

   @Override
   public void tweenEnd()
   {
      super.tweenEnd();
      mValue.set(mEnd);
   }
}
