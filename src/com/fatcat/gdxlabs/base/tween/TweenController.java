package com.fatcat.gdxlabs.base.tween;

import com.fatcat.gdxlabs.base.GameWorld;
import com.fatcat.gdxlabs.base.IUpdateable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tom on 26/04/2015.
 */
public class TweenController implements IUpdateable
{
   private List<ITween> mTweens;
   private List<ITween> mAddTweens;
   private List<ITween> mRemoveTweens;
   private boolean mLoopLocked = false; // variable that controls adding directly to tween list, or using caching lists

   public TweenController()
   {
      mTweens = new ArrayList<ITween>();
      mAddTweens = new ArrayList<ITween>();
      mRemoveTweens = new ArrayList<ITween>();
   }

   public void registerTween(ITween tween)
   {
      if(mLoopLocked)
      {
         mAddTweens.add(tween);
      }
      else
      {
         mTweens.add(tween);
         tween.setActive(true);
         tween.tweenStart();
      }
   }

   public void deregisterTween(ITween tween)
   {
      // mark that this tween has been deregistered, so later callbacks can know this happened
      tween.setActive(false);
      if(mLoopLocked)
         mRemoveTweens.add(tween);
      else
      {
         mTweens.remove(tween);
         mAddTweens.remove(tween); // just in case - it could happen! Think about it!
      }
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // lock adding/removing from main tween list!
      mLoopLocked = true;
      for(int i = mTweens.size()-1; i >= 0; --i)
      {
         ITween tween = mTweens.get(i);
         tween.update(world, delta);
         if(tween.isTweenFinished())
         {
            tween.setActive(false);
            tween.tweenEnd();
            mTweens.remove(i);
         }
      }
      mLoopLocked = false; // unlock adding/removing
      // add all new tweens
      for(ITween tween : mAddTweens)
         registerTween(tween);
      mAddTweens.clear();
      // remove all removed tweens
      for(ITween tween : mRemoveTweens)
         deregisterTween(tween);
      mRemoveTweens.clear();
   }

   public void clear()
   {
      mTweens.clear();
   }
}
