package com.fatcat.gdxlabs.base.tween;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.base.Utils;

/**
 * Created by Tom on 26/04/2015.
 */
public class Vector2Tween extends Tween<Vector2>
{
   public Vector2Tween(Vector2 val, Vector2 start, Vector2 end, float time, Interpolation interpolation)
   {
      super(val, start, end, time, interpolation);
   }

   public Vector2Tween(Vector2 val, Vector2 start, Vector2 end, float time)
   {
      super(val, start, end, time);
   }

   public Vector2Tween(Vector2 start, Vector2 end, float time, Interpolation interpolation)
   {
      super(start, end, time, interpolation);
   }

   public Vector2Tween(Vector2 start, Vector2 end, float time)
   {
      super(start, end, time);
   }

   @Override
   public void tweenStart()
   {
      super.tweenStart();
      mValue.set(mStart);
   }

   @Override
   public void tweenEnd()
   {
      super.tweenEnd();
      mValue.set(mEnd);
   }

   @Override
   public void tween(float alpha)
   {
      mValue.set(
            Utils.lerp(mStart.x, mEnd.x, alpha),
            Utils.lerp(mStart.y, mEnd.y, alpha)
      );
   }

   @Override
   public Vector2 defaultVal()
   {
      return Vector2.Zero.cpy();
   }
}
