package com.fatcat.gdxlabs.base.tween;

import com.fatcat.gdxlabs.base.GameWorld;

/**
 * Created by Tom on 26/04/2015.
 */
public class SeqTween extends BaseTween
{
   private BaseTween[] mTweens;
   private int mIndex;

   private boolean mRepeat = false;

   public SeqTween(BaseTween... tweens)
   {
      mTweens = tweens;
      mIndex = 0;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      if(!isTweenFinished())
      {
         mTweens[mIndex].update(world, delta);
         if(mTweens[mIndex].isTweenFinished())
         {
            mTweens[mIndex].tweenEnd();
            mIndex++;
            // If tween just ended and we are set to repeat, reset
            if(isTweenFinished())
            {
               if(mRepeat)
                  tweenStart(); // restart
            }
            else // start the next tween
            {
               mTweens[mIndex].tweenStart();
            }
         }
      }
   }

   @Override
   public void tweenStart()
   {
      super.tweenStart();
      mIndex = 0;
      mTweens[mIndex].tweenStart();
   }

   @Override
   public void tweenEnd()
   {
      super.tweenEnd();
   }

   @Override
   public boolean isTweenFinished()
   {
      return mIndex >= mTweens.length;
   }

   public boolean repeat()
   {
      return mRepeat;
   }

   public void repeat(boolean newRepeat)
   {
      mRepeat = newRepeat;
   }

   public ITween get(int index)
   {
      if(index < 0 || index >= mTweens.length)
         throw new IndexOutOfBoundsException("index = " + index + ", size = " + mTweens.length);
      return mTweens[index];
   }
}
