package com.fatcat.gdxlabs.base.tween;

import com.badlogic.gdx.math.Interpolation;
import com.fatcat.gdxlabs.base.GameWorld;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 17/05/2015.
 */
public class Timer extends FloatTween
{
   private boolean mRepeat;

   public Timer(float duration, boolean repeat)
   {
      super(0f, 1f, duration, Interpolation.linear);
      mRepeat = repeat;
   }

   public Timer(float duration, boolean repeat, TimerCallback cb)
   {
      this(duration, repeat);
      callbacks.add(cb);
   }

   @Override
   public void tweenStart()
   {
      mElapsedTime = 0;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      super.update(world, delta);
      if(mElapsedTime >= mTime && mRepeat)
      {
         tweenEnd();
         tweenStart();
      }
   }
   @Override
   public boolean isTweenFinished()
   {
      return super.isTweenFinished() && !mRepeat;
   }

   public float getElapsedTime()
   {
      return mElapsedTime;
   }

   public void setElapsedTime(float time)
   {
      mElapsedTime = time;
   }
}
