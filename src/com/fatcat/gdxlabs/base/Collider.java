package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 12/02/2015.
 */
public class Collider
{
   private Rectangle mRectangle;
   private Circle mCircle;

   // Constructor to create rectangle
   public Collider(Vector2 position, Vector2 dimensions)
   {
      mRectangle = new Rectangle(position.x, position.y, dimensions.x, dimensions.y);
   }

   public Collider(Rectangle rectangle)
   {
      mRectangle = new Rectangle(rectangle);
   }

   // Constructor to create circle
   public Collider(Vector2 position, float radius)
   {
      mCircle = new Circle(position, radius);
   }

   public Rectangle rect()
   {
      return mRectangle;
   }

   public Circle circle()
   {
      return mCircle;
   }
}
