package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 12/11/2015.
 */
public class FontDrawable extends Drawable
{
   private BitmapFont mFont;
   private Color mColour = Color.WHITE.cpy();
   private String mText = "";

   public FontDrawable(GameObject object, BitmapFont font)
   {
      super(object);
      mFont = font;
   }

   public void setFont(BitmapFont font)
   {
      mFont = font;
   }

   public BitmapFont getFont()
   {
      return mFont;
   }

   public void setText(String text)
   {
      mText = text;
   }

   public String getText()
   {
      return mText;
   }

   public void setColour(Color c)
   {
      mColour.set(c);
   }

   public Color getColour()
   {
      return mColour;
   }

   public void setAlpha(float a)
   {
      mColour.a = a;
   }

   public float getAlpha()
   {
      return mColour.a;
   }

   @Override
   public void draw(GameWorld world, SpriteBatch batch, Vector2 position)
   {
      mFont.setColor(mColour);
      // subtract height from draw position to correct for offset of BitmapFont draw position (damn it LibGDX!)
      mFont.draw(batch, mText, position.x, position.y + mFont.getBounds(mText).height);
      mFont.setColor(mColour.WHITE);
   }

   @Override
   public Vector2 getDrawBounds(Vector2 boundsOut)
   {
      BitmapFont.TextBounds bounds = mFont.getBounds(mText);
      boundsOut.set(bounds.width, bounds.height);
      return boundsOut;
   }

}
