package com.fatcat.gdxlabs.base.states;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.base.Drawable;
import com.fatcat.gdxlabs.base.FontDrawable;
import com.fatcat.gdxlabs.base.GameWorld;
import com.fatcat.gdxlabs.base.MultiDrawable;

/**
 * Created by Tom on 17/05/2015.
 */
public abstract class TextButton extends Button
{
   private MultiDrawable mMultiDrawable;
   private FontDrawable mFontDrawable;
   private String mText;

   public TextButton(Vector2 position, TextureRegion unselected, TextureRegion selected, BitmapFont font, String text)
   {
      super(position, unselected, selected);
      mText = text;
      mFontDrawable =  new FontDrawable(this, font);
      mFontDrawable.setText(mText);
      mMultiDrawable = new MultiDrawable(this, mDrawable, mFontDrawable);
   }

   public String getText() { return mText; }
   public void setText(String newText)
   {
      mText = newText;
      mFontDrawable.setText(mText);
   }

   @Override
   public Drawable getDrawable()
   {
      return mMultiDrawable;
   }
}
