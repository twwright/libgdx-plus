package com.fatcat.gdxlabs.base.states;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.base.GameWorld;

/**
 * Created by Tom on 15/02/2015.
 */
public class StateChangeButton extends Button
{
   private String mNewStateName;
   private Sound mSound;

   public StateChangeButton(Vector2 position, TextureRegion unselected, TextureRegion selected, String stateName, Sound sound)
   {
      super(position, unselected, selected);
      mNewStateName = stateName;
      mSound = sound;
   }

   @Override
   public void onPress(GameWorld world)
   {
      world.getStateManager().setNewState(mNewStateName);
      if(mSound != null)
         mSound.play();
   }
}
