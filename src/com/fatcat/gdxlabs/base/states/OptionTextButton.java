package com.fatcat.gdxlabs.base.states;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.base.GameWorld;
import com.fatcat.gdxlabs.base.Utils;

/**
 * Created by Tom on 19/08/2015.
 */
public class OptionTextButton extends TextButton
{
   private String[] mOptions;
   private int mOptionIndex = 0;

   public OptionTextButton(Vector2 position, TextureRegion unselected, TextureRegion selected, BitmapFont font, String[] options)
   {
      super(position, unselected, selected, font, options[0]);
      mOptions = options;
   }

   @Override
   public void onPress(GameWorld world)
   {
      next();
   }

   public void next()
   {
      mOptionIndex = (mOptionIndex == mOptions.length-1) ? 0 : mOptionIndex + 1;
      refresh();
   }

   public void prev()
   {
      mOptionIndex = (mOptionIndex == 0) ? mOptions.length-1 : mOptionIndex - 1;
      refresh();
   }

   public void refresh()
   {
      setText(mOptions[mOptionIndex]);
   }

   public String[] options()
   {
      return mOptions;
   }

   public void options(String[] options)
   {
      mOptions = options;
      mOptionIndex = Utils.clamp(mOptionIndex, 0, options.length - 1);
      refresh();
   }

   public void setCurrentOption(int index)
   {
      mOptionIndex = Utils.clamp(index, 0, mOptions.length-1);
      refresh();
   }
}
