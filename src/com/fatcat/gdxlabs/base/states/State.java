package com.fatcat.gdxlabs.base.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fatcat.gdxlabs.base.GameWorld;

/**
 * Created by Tom on 15/02/2015.
 */
public abstract class State
{
   private String mName;

   public State(String name)
   {
      mName = name;
   }

   public String getName()
   {
      return mName;
   }

   public void preDraw(SpriteBatch batch, GameWorld world)
   {
      // Nothing
   }

   public abstract void update(GameWorld world, float delta);
   public abstract void draw(SpriteBatch batch, GameWorld world);
   public abstract void enter(State oldState, GameWorld world);
   public abstract void exit(State newState, GameWorld world);
}
