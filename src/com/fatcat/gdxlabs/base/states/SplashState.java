package com.fatcat.gdxlabs.base.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fatcat.gdxlabs.base.GameSprite;
import com.fatcat.gdxlabs.base.GameWorld;
import com.fatcat.gdxlabs.base.tween.*;

/**
 * Created by Tom on 13/11/2015.
 */
public class SplashState extends State
{
   private Sprite mSprite;
   private GameSprite mOverlay;
   private Color mBackground;
   private BaseTween mIntroTween;
   private BaseTween mOutroTween;
   private ITweenCallback mTimerCallback;
   private float mDisplayTime;
   private Timer mTimer;
   private boolean mDetectTouch;

   public SplashState(String name, Sprite sprite, Color background, GameSprite overlay, boolean detectTouch, float displayTime, BaseTween introTween, BaseTween outroTween)
   {
      super(name);
      mSprite = sprite;
      mOverlay = overlay;
      mBackground = background;
      mIntroTween = introTween;
      mOutroTween = outroTween;
      mDisplayTime = displayTime;
      mDetectTouch = detectTouch;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      if(mDetectTouch && mTimer.isActive() && Gdx.input.isTouched())
      {
         world.getTweenController().deregisterTween(mTimer);
         world.getTweenController().registerTween(mOutroTween);
      }
   }

   @Override
   public void preDraw(SpriteBatch batch, GameWorld world)
   {
      // clear with background color
      Gdx.gl.glClearColor(mBackground.r, mBackground.g, mBackground.b, mBackground.a);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
      // draw splash sprite
      mSprite.draw(batch);
   }

   @Override
   public void draw(SpriteBatch batch, GameWorld world)
   {
      // drawing done in predraw
   }

   @Override
   public void enter(State oldState, final GameWorld world)
   {
      // add the overlay
      world.add(mOverlay);

      // set up the timer to start up outro
      mTimer = new Timer(mDisplayTime, false, new TimerCallback() {
         @Override
         public void onTimerFinish()
         {
            world.getTweenController().registerTween(mOutroTween);
         }
      });

      // register the intro tween, and add in our own callback
      mTimerCallback = new TweenCallback() {
         @Override
         public void onTweenFinish(ITween tween)
         {
            world.getTweenController().registerTween(mTimer);
         }
      };
      mIntroTween.callbacks.add(mTimerCallback);
      world.getTweenController().registerTween(mIntroTween);
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      // remove the callback we added to the intro tween
      mIntroTween.callbacks.remove(mTimerCallback);

      // remove the overlay
      world.remove(mOverlay);
   }
}
