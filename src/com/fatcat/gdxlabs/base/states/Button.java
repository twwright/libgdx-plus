package com.fatcat.gdxlabs.base.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.fatcat.gdxlabs.base.*;
import com.fatcat.gdxlabs.base.tween.IEventListener;

import java.util.ArrayList;

/**
 * Created by Tom on 15/02/2015.
 */
public abstract class Button extends GameObject implements IUpdateable, IDrawable
{
   protected SpriteDrawable mDrawable;
   private Sprite mSprite;
   private TextureRegion mRegionUnselected;
   private TextureRegion mRegionSelected;
   private boolean mSelected = false;
   private ArrayList<IEventListener> mListeners = new ArrayList<IEventListener>();

   public Button(Vector2 position, TextureRegion unselected, TextureRegion selected)
   {
      super(position);
      mRegionUnselected = unselected;
      mRegionSelected = selected;
      mSprite = new Sprite(unselected);
      mDrawable = new SpriteDrawable(this, mSprite);
   }

   public void setSelected(boolean selected)
   {
      mSelected = selected;
      mDrawable.getSprite().setRegion(mSelected ? mRegionSelected : mRegionUnselected);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      if(Gdx.input.isTouched())
      {
         // Retrieve touch coordinates and unproject with camera
         Vector3 screenCoords = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
         world.getCamera().unproject(screenCoords);

         Vector2 worldCoords = new Vector2(screenCoords.x, screenCoords.y);

         // Check if transformed screen coordinates are inside the button sprite
         Rectangle rect = mDrawable.getSprite().getBoundingRectangle();
         this.setSelected(rect.contains(worldCoords));
      }
      else
      {
         if(mSelected)
         {
            onPress(world);
            // notify listeners
            for(IEventListener listener : mListeners)
               listener.onEvent("ON_PRESS", this);
            mSelected = false;
         }
      }
   }

   public ArrayList<IEventListener> listeners() { return mListeners; }

   public Sprite getSprite()
   {
      return mSprite;
   }

   @Override
   public Drawable getDrawable()
   {
      return mDrawable;
   }

   public abstract void onPress(GameWorld world);
}
