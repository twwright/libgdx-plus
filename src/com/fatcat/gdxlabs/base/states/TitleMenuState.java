package com.fatcat.gdxlabs.base.states;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fatcat.gdxlabs.base.GameSprite;
import com.fatcat.gdxlabs.base.GameWorld;

import java.util.List;

/**
 * Created by Tom on 23/02/2015.
 */
public class TitleMenuState extends MenuState
{
   private GameSprite mTitle;
   private GameSprite mBackground;

   public TitleMenuState(String name, GameSprite background, List<Button> buttons, GameSprite title)
   {
      super(name, Color.WHITE, buttons);
      mTitle = title;
      mBackground = background;
   }

   public TitleMenuState(String name, Color background, List<Button> buttons, GameSprite title)
   {
      super(name, background, buttons);
      mTitle = title;
   }

   @Override
   public void draw(SpriteBatch batch, GameWorld world)
   {
      super.draw(batch, world);
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      if(mBackground != null)
         world.remove(mBackground);
      super.exit(newState, world);
      if(mTitle != null)
         world.remove(mTitle);
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      if(mBackground != null)
         world.add(mBackground);
      super.enter(oldState, world);
      if(mTitle != null)
         world.add(mTitle);
   }
}
