package com.fatcat.gdxlabs.base.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.base.GameWorld;

/**
 * Created by Tom on 15/02/2015.
 */
public class ExitButton extends Button
{
   private Sound mSound;
   public ExitButton(Vector2 position, TextureRegion unselected, TextureRegion selected, Sound sound)
   {
      super(position, unselected, selected);
      mSound = sound;
   }

   @Override
   public void onPress(GameWorld world)
   {
      if(mSound != null)
         mSound.play();
      // Quit the com.fatcat.gdxlabs.game!
      Gdx.app.exit();
   }
}
