package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 11/02/2015.
 */
public class GameObject
{
   protected final Vector2 mPosition = new Vector2(0, 0);
   protected boolean mActive = true;

   public GameObject(Vector2 position)
   {
      mPosition.set(position);
   }

   public Vector2 position()
   {
      return mPosition;
   }

   public Vector2 position(Vector2 newPosition)
   {
      mPosition.set(newPosition);
      return mPosition;
   }

   public boolean isActive()
   {
      return mActive;
   }

   public void setActive(boolean active)
   {
      mActive = active;
   }

   public void onAdd(GameWorld world)
   {

   }

   public void onRemove(GameWorld world)
   {

   }
}
