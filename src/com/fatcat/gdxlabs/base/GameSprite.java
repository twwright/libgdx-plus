package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 23/02/2015.
 */
public class GameSprite extends GameObject implements IDrawable
{
   protected SpriteDrawable mDrawable;

   public GameSprite(Vector2 position, TextureRegion texture)
   {
      this(position, new Sprite(texture));
   }

   public GameSprite(Vector2 position, TextureRegion texture, float zDepth)
   {
      this(position, texture);
      mDrawable.setZDepth(zDepth);
   }
   public GameSprite(Vector2 position, Sprite sprite)
   {
      super(position);
      mDrawable = new SpriteDrawable(this, sprite);
   }


   @Override
   public SpriteDrawable getDrawable()
   {
      return mDrawable;
   }
}
