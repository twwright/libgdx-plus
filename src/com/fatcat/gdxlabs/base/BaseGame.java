package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.base.states.StateManager;
import com.fatcat.gdxlabs.base.tween.EventManager;
import com.fatcat.gdxlabs.base.tween.TweenController;

import java.util.List;

/**
 * Created by Tom on 12/02/2015.
 */
public abstract class BaseGame extends ApplicationAdapter
{
   public static boolean colliderDrawEnabled = false;
   protected SpriteBatch mBatch;
   protected ShapeRenderer mRenderer; // For drawing primitives - used to debug the colliders - see below
   protected StateManager mStateManager;
   protected Camera mCamera;
   protected Camera mUICamera;
   protected GameWorld mWorld;
   protected Collision mCollision;
   protected TweenController mTweenController;
   protected EventManager mEventManager;
   protected Color mClearColour = Color.WHITE;

   public Color clearColour()
   {
      return mClearColour;
   }

   public void clearColour(Color color)
   {
      mClearColour.set(color);
   }

   @Override
   public void create ()
   {
      mBatch = new SpriteBatch();
      mRenderer = new ShapeRenderer();
      mStateManager = new StateManager();
      mCamera = new OrthographicCamera(800, 480);
      mCamera.position.set(mCamera.viewportWidth/2, mCamera.viewportHeight/2, 0);
      mUICamera = new OrthographicCamera(800, 480);
      mUICamera.position.set(mUICamera.viewportWidth/2, mUICamera.viewportHeight/2, 0);
      mTweenController = new TweenController();
      mEventManager = new EventManager();
      mWorld = new GameWorld(new Vector2(mCamera.viewportWidth, mCamera.viewportHeight), mCamera, mStateManager, mTweenController, mEventManager);
      mCollision = new RectangleCollision();
      // Call onCreate (implemented by subclass)
      onCreate();
      // Initialise game
      onInitialiseGame();
   }

   @Override
   public void render ()
   {
      Gdx.gl.glClearColor(mClearColour.r, mClearColour.g, mClearColour.b, mClearColour.a);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      if(Gdx.input.isKeyJustPressed(Input.Keys.F1) || !mWorld.isRunning())
         onInitialiseGame(); // Reset

      // Allow the state manager to change com.fatcat.gdxlabs.game.states if necessary
      mStateManager.changeState(mWorld);

      // Update the current state
      if(mStateManager.getCurrentState() != null)
         mStateManager.getCurrentState().update(mWorld, Gdx.graphics.getDeltaTime());

      // Update the game world and camera
      update();
      mTweenController.update(mWorld, Gdx.graphics.getDeltaTime());
      mCamera.update();

      // update ui camera to match size of camera
      mUICamera.viewportWidth = mCamera.viewportWidth;
      mUICamera.viewportHeight = mCamera.viewportHeight;
      mUICamera.position.set(mUICamera.viewportWidth/2, mUICamera.viewportHeight/2, 0);
      mUICamera.update();

      // Add and remove objects designated in update
      mWorld.performUpdateAddRemove();

      // Collision
      mCollision.collision(mWorld.getCollideables(), mWorld);

      // Draw state (pre-world)
      if(mStateManager.getCurrentState() != null)
      {
         mBatch.setProjectionMatrix(mCamera.combined);
         mBatch.begin();
         mStateManager.getCurrentState().preDraw(mBatch, mWorld);
         mBatch.end();
      }
      // Draw the com.fatcat.gdxlabs.game world
      draw();
      // Draw the current state
      if(mStateManager.getCurrentState() != null)
      {
         mBatch.setProjectionMatrix(mCamera.combined);
         mBatch.begin();
         mStateManager.getCurrentState().draw(mBatch, mWorld);
         mBatch.end();
      }
   }

   public void update()
   {
      List<IUpdateable> updateables = mWorld.getUpdateables();
      for(IUpdateable updateable : updateables)
      {
         if(((GameObject)updateable).isActive())
            updateable.update(mWorld, Gdx.graphics.getDeltaTime());
      }
   }

   public void draw()
   {
      // retrieve drawables and perform insertion sort
      List<IDrawable> drawables = mWorld.getDrawables();
      for(int i = 0; i < drawables.size(); ++i) // element to sort
      {
         for(int j = i-1; j >= 0 ; --j) // backwards over sorted elements
         {
            IDrawable iDrawable = drawables.get(j+1); // the sorting element
            IDrawable jDrawable = drawables.get(j); // sorted element
            if(iDrawable.getDrawable().getZDepth() > jDrawable.getDrawable().getZDepth())
            {
               drawables.set(j+1, jDrawable);
               drawables.set(j, iDrawable);
            }
            else
            {
               // correct position!
               break;
            }
         }
      }

      // draw objects
      mBatch.setProjectionMatrix(mCamera.combined);
      boolean isProjected = true;
      mBatch.begin();
      for(IDrawable iDrawable : drawables)
      {
         Drawable drawable = iDrawable.getDrawable();
         if(((GameObject)iDrawable).isActive())
         {
            if(drawable.isCameraProjected())
            {
               if(!isProjected)
                  mBatch.setProjectionMatrix(mCamera.combined);
               isProjected = true;
            }
            else
            {
               if(isProjected)
                  mBatch.setProjectionMatrix(mUICamera.combined);
               isProjected = false;
            }
            drawable.draw(mWorld, mBatch);
         }
      }
      mBatch.end();

      // Debug drawing of colliders
      if(colliderDrawEnabled)
      {
         mRenderer.setProjectionMatrix(mCamera.combined);
         mRenderer.begin(ShapeRenderer.ShapeType.Line);
         for (ICollidable collideable : mWorld.getCollideables()) {
            if (((GameObject) collideable).isActive())
               mCollision.drawCollider(mRenderer, collideable);
         }
         mRenderer.end();
      }
   }

   public abstract void onCreate();
   public abstract void onInitialiseGame();
}
