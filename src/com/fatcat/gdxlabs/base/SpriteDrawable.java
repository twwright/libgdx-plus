package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 12/11/2015.
 */
public class SpriteDrawable extends Drawable
{
   private Sprite mSprite;

   public SpriteDrawable(GameObject object, Sprite sprite)
   {
      super(object);
      mSprite = sprite;
   }

   public Sprite getSprite()
   {
      return mSprite;
   }

   @Override
   public void draw(GameWorld world, SpriteBatch batch, Vector2 position)
   {
      mSprite.setPosition(position.x, position.y);
      mSprite.draw(batch);
   }

   @Override
   public Vector2 getDrawBounds(Vector2 boundsOut)
   {
      boundsOut.set(mSprite.getWidth(), mSprite.getHeight());
      return boundsOut;
   }
}
