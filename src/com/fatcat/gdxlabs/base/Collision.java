package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.List;

/**
 * Created by Tom on 15/02/2015.
 */
public abstract class Collision
{
   public void collision(List<ICollidable> collideables, GameWorld world)
   {
      for(int i = 0; i < collideables.size(); ++i)
      {
         ICollidable first = collideables.get(i);
         // No need to
         if(((GameObject)first).isActive())
         {
            // Test collideable with EACH OTHER collideable!
            for(int j = i+1; j < collideables.size(); ++j)
            {
               ICollidable second = collideables.get(j);
               // Only perform collision check if at least one is not static, and both active
               if((!first.isStatic() || !second.isStatic()) && ((GameObject)second).isActive())
               {
                  boolean colliding = collisionCheck(first.getCollider(), second.getCollider());
                  if(colliding)
                  {
                     // Do we need to resolve the collision? Check that both are solid
                     if(first.isSolid() && second.isSolid())
                     {
                        // Move colliders to not be colliding anymore!
                        resolveCollision(first, second);
                     }
                     // Call collision method of each collideable - let the com.fatcat.gdxlabs.game object respond to the collision
                     first.onCollide(second, world);
                     second.onCollide(first, world);
                     if(!((GameObject)first).isActive())
                        break; // First is now no longer active, should stop checking collisions with it
                  }
               }
            }
         }
      }
   }

   public abstract boolean collisionCheck(Collider first, Collider second);

   public abstract void resolveCollision(ICollidable first, ICollidable second);

   public abstract void drawCollider(ShapeRenderer renderer, ICollidable collideable);
}
