package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 15/03/2015.
 */
public class Utils
{
   public static final Vector2 Left = new Vector2(-1, 0);
   public static final Vector2 Right = new Vector2(1, 0);
   public static final Vector2 Up = new Vector2(0, 1);
   public static final Vector2 Down = new Vector2(0, -1);
   
   public static float rand()
   {
      return (float)Math.random();
   }

   public static double rand(double min, double max) { return min + Math.random() * (max-min); }
   public static float rand(float min, float max)
   {
      return min + (float)(Math.random() * (max-min));
   }

   public static int rand(int min, int max)
   {
      return min + (int)(Math.random() * (max-min));
   }

   public static int rand(int max)
   {
      return rand(0, max);
   }

   public static <T> T rande(T[] arr)
   {
      return arr[rand(arr.length)];
   }

   public static float lerp(float start, float end, float alpha)
   {
      return start + (end-start) * alpha;
   }

   public static float clamp(float val, float min, float max) { return (val < min) ? min : ((val > max) ? max : val); }

   public static int clamp(int val, int min, int max) { return (val < min) ? min : ((val > max) ? max: val); }

   /**
    * Calculates the alpha and beta values for the intersection of two lines
    * If the line segments intersect, alpha and beta will be in the range [0, 1]
    * @param a0 - Start point of line A
    * @param a1 - End point of line A
    * @param b0 - Start point of line B
    * @param b1 - End point of line B
    * @return {Vector2} null if lines are parallel, or x element contains alpha value (for parametric form of A), y element contains beta value (for parametric form of B)
    */
   public static Vector2 lineInterect(Vector2 a0, Vector2 a1, Vector2 b0, Vector2 b1)
   {
      a1 = a1.cpy().sub(a0); // Convert
      b1 = b1.cpy().sub(b0);
      Vector2 a1perp = new Vector2(-a1.y, a1.x); // Create perpendicular vectors
      Vector2 b1perp = new Vector2(-b1.y, b1.x);
      Vector2 aSubB = a0.cpy().sub(b0);   // Create sub vectors
      Vector2 bSubA = b0.cpy().sub(a0);
      float a1pDotB1 = a1perp.dot(b1);
      float b1pDotA1 = b1perp.dot(a1);
      Vector2 result = null;
      if(a1pDotB1 != 0 && b1pDotA1 != 0) // Check denominators are realistic
      {
         float beta = a1perp.dot(aSubB) / a1pDotB1;
         float alpha = b1perp.dot(bSubA) / b1pDotA1;
         result = new Vector2(alpha, beta);
      }
      return result;
   }

   public static Vector2 circleLineIntersect(Vector2 c, float r, Vector2 a, Vector2 b)
   {
      // test line-circle intersection
      Vector2 ab = b.cpy().sub(a);
      float abLen = ab.len(); // length of line
      ab.nor(); // ab is now normalised direction of line
      Vector2 ac = c.cpy().sub(a); // vector from start of line to circle
      Vector2 bc = c.cpy().sub(b); // vector from end of line to circle
      float acLen = ac.len(); // length of start->circle
      float bcLen = bc.len(); // length of end->circle
      float projCircleLine = ac.dot(ab); // projection of start->circle vector onto line
      float projCircleEndLine = -bc.dot(ab); // projection of end->circle onto line
      float distSquared = acLen * acLen - projCircleLine * projCircleLine; // perpendicular distance from line, squared (pythagoras)
      if(projCircleLine + Math.abs(projCircleLine) * r / acLen >= 0
            && projCircleEndLine + Math.abs(projCircleEndLine) * r / bcLen >= 0 // circle with bounds along the line
            && distSquared < r * r) // circle within radius perpendicular to line
      {
         float lineAlpha = Utils.clamp(projCircleLine, 0, abLen);
         Vector2 intersectPos = a.cpy().add(ab.scl(lineAlpha));
         return intersectPos;
      }
      return null;
   }

   public static Vector2 reflect(Vector2 v, Vector2 normal)
   {
      v.sub(normal.cpy().scl(2 * v.dot(normal)));
      return v;
   }

   public static Vector2 randArc(Vector2 center, float arcRadians)
   {
      float diverge = (float)(-arcRadians/2 + Math.random() * arcRadians);
      float angle = (float)Math.atan2(center.y, center.x) + diverge;
      return new Vector2((float)Math.cos(angle), (float)Math.sin(angle));
   }

   public class Pair<K, V>
   {
      private K mKey;
      private V mValue;

      public Pair(K key, V value)
      {
         mKey = key;
         mValue = value;
      }

      public K getKey()
      {
         return mKey;
      }

      public V getValue()
      {
         return mValue;
      }

      public void setKey(K newKey)
      {
         mKey = newKey;
      }

      public void setValue(V newValue)
      {
         mValue = newValue;
      }
   }
}
