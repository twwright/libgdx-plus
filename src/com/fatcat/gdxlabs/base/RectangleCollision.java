package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 4/06/2015.
 */
public class RectangleCollision extends Collision
{
   @Override
   public boolean collisionCheck(Collider firstCollider, Collider secondCollider)
   {
      Rectangle first = firstCollider.rect(), second = secondCollider.rect();

      // First max is greater than second min?
      boolean firstGreater = first.getX() + first.getWidth() > second.getX()
            && first.getY() + first.getHeight() > second.getY();

      // Second max is greater than first min?
      boolean secondGreater = second.getX() + second.getWidth() > first.getX()
            && second.getY() + second.getHeight() > first.getY();

      return firstGreater && secondGreater;

      // Using LibGDX Rectangle methods!
      // return first.overlaps(second);
   }

   @Override
   public void resolveCollision(ICollidable firstCollideable, ICollidable secondCollideable)
   {
      Rectangle first = firstCollideable.getCollider().rect(), second = secondCollideable.getCollider().rect();

      Vector2 firstMin = new Vector2(first.getX(), first.getY());
      Vector2 firstMax = new Vector2(first.getX() + first.getWidth(), first.getY() + first.getHeight());
      Vector2 secondMin = new Vector2(second.getX(), second.getY());
      Vector2 secondMax = new Vector2(second.getX() + second.getWidth(), second.getY() + second.getHeight());

      // Distance to separate with 'first' collider moving in the direction specified
      float separateLeft = secondMin.x - firstMax.x;
      float separateRight = secondMax.x - firstMin.x;
      float separateTop = secondMax.y - firstMin.y;
      float separateBottom = secondMin.y - firstMax.y;

      // Determine smallest overlap in each axis
      Vector2 separate = new Vector2(
            Math.abs(separateLeft) < Math.abs(separateRight) ? separateLeft : separateRight,
            Math.abs(separateTop) < Math.abs(separateBottom) ? separateTop : separateBottom
      );

      // Determine smallest overlap axis
      if(Math.abs(separate.x) < Math.abs(separate.y))
         separate.y = 0;
      else
         separate.x = 0;

      // Now use separate vector to resolve collision
      if(firstCollideable.isStatic())
      {
         // Overlap is vector to move first collider - so reverse to move second
         // Move second full distance
         separate.scl(-1);
         second.setX(second.getX() + separate.x);
         second.setY(second.getY() + separate.y);
      }
      else if(secondCollideable.isStatic())
      {
         // Move first full distance
         first.setX(first.getX() + separate.x);
         first.setY(first.getY() + separate.y);
      }
      else
      {
         // Move each collider half distance
         first.setX(first.getX() + separate.x * 0.5f);
         first.setY(first.getY() + separate.y * 0.5f);
         second.setX(second.getX() + -separate.x * 0.5f);
         second.setY(second.getY() + -separate.y * 0.5f);
      }

      // Update com.fatcat.gdxlabs.game object positions to reflect changes in collider position
      ((GameObject)firstCollideable).position(firstCollideable.getCollider().rect().getCenter(new Vector2()));
      ((GameObject)secondCollideable).position(secondCollideable.getCollider().rect().getCenter(new Vector2()));
   }

   @Override
   public void drawCollider(ShapeRenderer renderer, ICollidable collideable)
   {
      Rectangle rect = collideable.getCollider().rect();
      renderer.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
   }
}
