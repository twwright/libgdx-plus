package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 12/11/2015.
 */
public class MultiDrawable extends Drawable
{
   private Drawable[] mDrawables;

   public MultiDrawable(GameObject object, Drawable... drawables)
   {
      super(object);
      mDrawables = drawables;
   }

   @Override
   public void draw(GameWorld world, SpriteBatch batch, Vector2 position)
   {
      for(int i = 0; i < mDrawables.length; ++i)
         mDrawables[i].draw(world, batch);
   }

   @Override
   public Vector2 getDrawBounds(Vector2 boundsOut)
   {
      return Vector2.Zero;
   }

   public Drawable getDrawable(int i)
   {
      return mDrawables[i];
   }
}
