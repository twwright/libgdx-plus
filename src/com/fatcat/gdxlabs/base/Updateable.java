package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 1/10/2015.
 */
public abstract class Updateable extends GameObject implements IUpdateable
{
   public Updateable()
   {
      super(Vector2.Zero);
   }

   public Updateable(Vector2 position)
   {
      super(position);
   }
}
