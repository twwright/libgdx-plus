package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 4/06/2015.
 */
public class CircleCollision extends Collision
{
   @Override
   public boolean collisionCheck(Collider firstCollider, Collider secondCollider)
   {
      Circle first = firstCollider.circle(), second = secondCollider.circle();

      float distance = first.center().dst(second.center());
      float radiusSum = first.radius() + second.radius();
      return distance <= radiusSum;
   }

   @Override
   public void resolveCollision(ICollidable firstCollideable, ICollidable secondCollideable)
   {
      Circle first = firstCollideable.getCollider().circle(), second = secondCollideable.getCollider().circle();

      // Calculate how much the circles are overlapping!
      float distance = first.center().dst(second.center());
      float radiusSum = first.radius() + second.radius();
      float overlap = radiusSum - distance;

      // A - B = B -> A (this gives a direction vector from second to first!
      Vector2 direction = first.center().cpy().sub(second.center()).nor();

      // Are both moving in the resolution, or just one (because the other is static)?
      if(firstCollideable.isStatic())
      {
         // Scale by -1 to REVERSE the direction of the vector - need to move AWAY from collision
         direction.scl(-1);
         // Non-static collider (second) moves away from collisoon the FULL distance of overlap
         second.center().add(direction.cpy().scl(overlap));
      }
      else if(secondCollideable.isStatic())
      {
         // Non-static collider (first) moves away from collision the FULL distance of overlap
         first.center().add(direction.cpy().scl(overlap));
      }
      else
      {
         // Each collider moves away from the collision HALF the distance of the overlap
         first.center().add(direction.cpy().scl(overlap * 0.5f));
         second.center().add(direction.cpy().scl(-overlap * 0.5f));
      }

      // Update the com.fatcat.gdxlabs.game object position to reflect changed collider center (resolution)
      ((GameObject)firstCollideable).position(firstCollideable.getCollider().circle().center());
      ((GameObject)secondCollideable).position(secondCollideable.getCollider().circle().center());
   }

   @Override
   public void drawCollider(ShapeRenderer renderer, ICollidable collideable)
   {
      Circle circle = collideable.getCollider().circle();
      renderer.circle(circle.center().x, circle.center().y, circle.radius());
   }
}
