package com.fatcat.gdxlabs.base;

/**
 * Created by Tom on 11/02/2015.
 */
public interface IUpdateable
{
   public void update(GameWorld world, float delta);
}
