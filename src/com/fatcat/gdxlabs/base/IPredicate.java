package com.fatcat.gdxlabs.base;

/**
 * Created by Tom on 28/06/2015.
 */
public interface IPredicate<T>
{
   public boolean evaluate(T o);
}
