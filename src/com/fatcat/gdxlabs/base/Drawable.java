package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 12/11/2015.
 */
public abstract class Drawable
{
   protected GameObject mObject;
   private boolean mIsCameraProjected = true;
   private float mZDepth = 0;
   protected Alignment mAlignment = Alignment.Center;
   private Vector2 mCustomOffset = Vector2.Zero.cpy();

   private Vector2 mTempPosition = Vector2.Zero.cpy();
   private Vector2 mTempBounds = Vector2.Zero.cpy();


   public Drawable(GameObject object)
   {
      mObject = object;
   }

   public void setCameraProjected(boolean isCameraProjected)
   {
      mIsCameraProjected = isCameraProjected;
   }

   public boolean isCameraProjected()
   {
      return mIsCameraProjected;
   }

   public void setZDepth(float z)
   {
      mZDepth = z;
   }

   public float getZDepth()
   {
      return mZDepth;
   }

   public Alignment getAlignment()
   {
      return mAlignment;
   }

   public void setAlignment(Alignment alignment)
   {
      mAlignment = alignment;
   }

   public Vector2 getCustomAlignment()
   {
      return mCustomOffset;
   }

   public void setCustomAlignment(Vector2 offset)
   {
      mCustomOffset.set(offset);
   }

   public void draw(GameWorld world, SpriteBatch batch)
   {
      mTempPosition.set(mObject.position());
      getDrawBounds(mTempBounds);
      float height = mTempBounds.y;
      float width = mTempBounds.x;

      switch(mAlignment)
      {
         case TopLeft:
            mTempPosition.y -= height;
            break;
         case MidLeft:
            mTempPosition.y -= height/2;
            break;
         case BottomLeft:
            // This is the default
            break;
         case TopMid:
            mTempPosition.x -= width/2;
            mTempPosition.y -= height;
            break;
         case Center:
            mTempPosition.x -= width/2;
            mTempPosition.y -= height/2;
            break;
         case BottomMid:
            mTempPosition.x -= width/2;
            mTempPosition.y += height;
            break;
         case TopRight:
            mTempPosition.x -= width;
            mTempPosition.y -= height;
            break;
         case MidRight:
            mTempPosition.x -= width;
            mTempPosition.y -= height/2;
            break;
         case BottomRight:
            mTempPosition.x -= width;
            break;
         case Custom:
            mTempPosition.x -= mCustomOffset.x;
            mTempPosition.y -= mCustomOffset.y;
      }

      draw(world, batch, mTempPosition);
   }

   public abstract void draw(GameWorld world, SpriteBatch batch, Vector2 position);

   public abstract Vector2 getDrawBounds(Vector2 boundsOut);

   public enum Alignment
   {
      TopLeft,
      MidLeft,
      BottomLeft,
      TopMid,
      Center,
      BottomMid,
      TopRight,
      MidRight,
      BottomRight,
      Custom
   }
}
