package com.fatcat.gdxlabs.base;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.base.states.StateManager;
import com.fatcat.gdxlabs.base.tween.EventManager;
import com.fatcat.gdxlabs.base.tween.TweenController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Tom on 11/02/2015.
 */
public class GameWorld
{
   // Main collection of all com.fatcat.gdxlabs.game objects
   private List<GameObject> mGameObjects = new ArrayList<GameObject>();

   // Lists to add and remove com.fatcat.gdxlabs.game objects in update
   private List<GameObject> mAddedGameObjects = new LinkedList<GameObject>();
   private List<GameObject> mRemovedGameObjects = new LinkedList<GameObject>();

   // Specific collection of each com.fatcat.gdxlabs.game object composition element
   private List<IUpdateable> mUpdateables = new ArrayList<IUpdateable>();
   private List<ICollidable> mCollideables = new ArrayList<ICollidable>();
   private List<IDrawable> mDrawables = new ArrayList<IDrawable>();

   // Other information that needs to be shared with com.fatcat.gdxlabs.game objects and com.fatcat.gdxlabs.game
   private Camera mCamera;
   private StateManager mStateManager;
   private TweenController mTweenController;
   private EventManager mEventManager;
   private boolean mIsRunning = true;

   // Game world size
   private Vector2 mWorldSize;

   public GameWorld(Vector2 size, Camera camera, StateManager manager, TweenController tweenController, EventManager eventManager)
   {
      mCamera = camera;
      mStateManager = manager;
      mTweenController = tweenController;
      mEventManager = eventManager;
      mWorldSize = size;
   }

   public void add(GameObject obj)
   {
      mGameObjects.add(obj);
      if(obj instanceof IUpdateable)
         mUpdateables.add((IUpdateable)obj);
      if(obj instanceof ICollidable)
         mCollideables.add((ICollidable)obj);
      if(obj instanceof IDrawable)
         mDrawables.add((IDrawable)obj);

      obj.onAdd(this);
      obj.setActive(true);
   }

   public <T extends GameObject> void addAll(Collection<T> collection)
   {
      for(T t : collection)
         add(t);
   }

   /*
    * Unused in these examples, but given as a reference!
    */
   public void remove(GameObject obj)
   {
      mGameObjects.remove(obj);

      if(obj instanceof IUpdateable)
         mUpdateables.remove((IUpdateable) obj);
      if(obj instanceof ICollidable)
         mCollideables.remove((ICollidable) obj);
      if(obj instanceof IDrawable)
         mDrawables.remove((IDrawable) obj);

      obj.onRemove(this);
      obj.setActive(false);
   }

   public void updateRemove(GameObject obj)
   {
      mRemovedGameObjects.add(obj);
   }

   public void updateAdd(GameObject obj)
   {
      mAddedGameObjects.add(obj);
   }

   public void performUpdateAddRemove()
   {
      for(GameObject obj : mRemovedGameObjects)
         remove(obj);
      mRemovedGameObjects.clear();
      for(GameObject obj : mAddedGameObjects)
         add(obj);
      mAddedGameObjects.clear();
   }

   /*
    * Unused in these examples, but given as a reference!
    */
   public void removeInactives()
   {
      for(int i = mGameObjects.size()-1; i >= 0; --i)
      {
         GameObject obj = mGameObjects.get(i);
         if(!obj.isActive())
         {
            mGameObjects.remove(i);
            if(obj instanceof IUpdateable)
               mUpdateables.remove((IUpdateable) obj);
            if(obj instanceof ICollidable)
               mCollideables.remove((ICollidable) obj);
            if(obj instanceof IDrawable)
               mDrawables.remove((IDrawable) obj);
         }
      }
   }

   public void removeAll()
   {
      mGameObjects.clear();
      mUpdateables.clear();
      mDrawables.clear();
      mCollideables.clear();
   }

   public GameObject find(IPredicate<GameObject> p)
   {
      for(GameObject go : mGameObjects)
      {
         if(p.evaluate(go) && !mRemovedGameObjects.contains(go))
            return go;
      }
      return null;
   }

   public List<GameObject> findAll(IPredicate<GameObject> p)
   {
      ArrayList<GameObject> objs = new ArrayList<GameObject>();
      for(GameObject go : mGameObjects)
      {
         if(p.evaluate(go) && !mRemovedGameObjects.contains(go))
            objs.add(go);
      }
      return objs;
   }
   /* Java 8 Version using streams
   public List<GameObject> findAll(Predicate<GameObject> p)
   {
      return mGameObjects.stream().filter(p).collect(Collectors.toList());
   }
   */

   public List<GameObject> getGameObjects() { return mGameObjects; }

   public List<IUpdateable> getUpdateables()
   {
      return mUpdateables;
   }

   public List<ICollidable> getCollideables()
   {
      return mCollideables;
   }

   public List<IDrawable> getDrawables()
   {
      return mDrawables;
   }

   public Vector2 size()
   {
      return mWorldSize;
   }

   public void setRunning(boolean running)
   {
      mIsRunning = running;
   }

   public boolean isRunning()
   {
      return mIsRunning;
   }

   public Camera getCamera()
   {
      return mCamera;
   }

   public StateManager getStateManager()
   {
      return mStateManager;
   }

   public TweenController getTweenController() { return mTweenController; }

   public EventManager getEventManager() { return mEventManager; }
}
